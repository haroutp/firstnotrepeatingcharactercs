﻿using System;
using System.Collections.Generic;

namespace FirstNotRepeatingCharacter
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "abacabaabacaba";
            string s1 = "abacabad";

            System.Console.WriteLine(firstNotRepeatingCharacter(s));
            System.Console.WriteLine(firstNotRepeatingCharacter(s1));
        }

        static char firstNotRepeatingCharacter(string s) {
            char firstNotRepeating = '_';
            Dictionary<char, int> characters = new Dictionary<char, int>();
            for (int i = 0; i < s.Length; i++)
            {
                if(characters.ContainsKey(s[i])){
                    characters[s[i]]++;
                }else{
                    characters[s[i]] = 1;
                }
            }
            foreach (var item in characters.Keys){
                if(characters[item] == 1){
                    firstNotRepeating = item;
                    break;
                }
            }
            return firstNotRepeating;
            
        }

    }
}
